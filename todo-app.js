(function() {
    // создаем и возвращаем заголовок приложения
    function createAppTitle(title) {
        let appTitle = document.createElement('h2');
        appTitle.innerHTML = title;
        return appTitle;
    }

    // создаем и возвращаем форму для создания дела
    function createTodoItemForm() {
        let form = document.createElement('form');
        let input = document.createElement('input');
        let buttonWrapper = document.createElement('div');
        let button = document.createElement('button');

        form.classList.add('input-group', 'mb-3');
        input.classList.add('form-control');
        input.placeholder = 'Введите название нового дела';
        buttonWrapper.classList.add('input-group-append');
        button.classList.add('btn', 'btn-primary');
        button.textContent = 'Добавить дело';

        buttonWrapper.append(button);
        form.append(input);
        form.append(buttonWrapper)

        return {
            form,
            input,
            button,
        };
    }

    // создаем и возвращаем список элементов
    function createTodoList() {
        let list = document.createElement('ul');
        list.classList.add('list-group');
        return list;
        
    }

    function createTodoItem(name, done = false, storage) {
        let item = document.createElement('li');
        // кнопки в элемент, который красиво покажет их в одной группе
        let buttonGroup = document.createElement('div');
        let doneButton = document.createElement('button');
        let deleteButton = document.createElement('button');

        // устанавливаем стили для элемента списка, а так же для размещения кнопок
        // в его правой части с помощью flex
        item.classList.add('list-group-item', 'd-flex', 'justify-content-between', 'align-items-center');
        done ? item.classList.add('list-group-item-success') : null;
        item.setAttribute('done', done);
        item.textContent = name;

        
        
        buttonGroup.classList.add('btn-group', 'btn-group-sm');
        doneButton.classList.add('btn', 'btn-success');
        doneButton.textContent = 'Готово';
        deleteButton.classList.add('btn', 'btn-danger');
        deleteButton.textContent = 'Удалить';

        // вкладываем кнопки в отдельный элемент, чтобы они объединились в один блок
        buttonGroup.append(doneButton);
        buttonGroup.append(deleteButton);
        item.append(buttonGroup);
        let doneStorage = (status) => {
            storageParse = JSON.parse(localStorage.getItem(storage));

            storageParse.forEach(itemArr => {
                Object.entries(itemArr).forEach(([key, value]) => {
                    if (item.textContent.startsWith(value)) {
                        itemArr.done = status;
                        localStorage[storage] = JSON.stringify(storageParse);
                    }
                });
            });
        } 
        doneButton.addEventListener('click', function() {
            item.classList.toggle('list-group-item-success');

            if (item.classList.contains('list-group-item-success')) {
                item.setAttribute('done', true);
                doneStorage(true);
            } 
            else { 
                item.setAttribute('done', false);
                doneStorage(false);
            }
        });
        
        deleteButton.addEventListener('click', function() {
            storageParse = JSON.parse(localStorage.getItem(storage));

            if (confirm('Вы уверены?')) {
                item.remove();
                storageParse.forEach((itemArr, index) => {
                    Object.entries(itemArr).forEach(([key, value]) => {
                        if (item.textContent.startsWith(value)) {
                            storageParse.splice(index, 1);
                            localStorage[storage] = JSON.stringify(storageParse);
                        }
                    });
                });
            }
        });

        // приложению нужен доступ к самому элементу и кнопками, чтобы обрабатывать события нажатия
        return {
            item,
            doneButton,
            deleteButton,
        }
    }
    

    function createTodoApp(container, title = 'Список дел', storage, arr = null) {
        /* arr = [
            {name: 'Купить трусы', done: false },
            {name: 'Покушать', done: true },
            {name: 'Купить порошок', done: false },
        ]; */
        
        
        let todoAppTitle = createAppTitle(title);
        let todoItemForm = createTodoItemForm();
        let todoList = createTodoList();

        container.append(todoAppTitle);
        container.append(todoItemForm.form);
        container.append(todoList);

        if (localStorage[storage] === null || localStorage[storage] === undefined) {
            localStorage.setItem(storage, '[]');
        } 

        let loadTodoItem = (array) => {
            if (array.length !== 0) {
                array.forEach(item => {
                    Object.keys(item).forEach(key => {
                        if (key === 'name') {
                            let todoItemArr = createTodoItem(item.name, item.done, storage);
                            todoList.append(todoItemArr.item);
                        }
                    });
                });
            }
        }

        let storageParse = JSON.parse(localStorage.getItem(storage));

        loadTodoItem(storageParse);

        if (arr !== null) {
            loadTodoItem(arr);
        }

        let checkInput = () => {
            !todoItemForm.input.value ?
            todoItemForm.button.setAttribute('disabled', '') :
            todoItemForm.button.removeAttribute('disabled');
        }
        checkInput();
        todoItemForm.input.addEventListener('input', checkInput);
        // браузер создает событие submit на форме по нажатию Enter или на кнопку создания дела
        todoItemForm.form.addEventListener('submit', function(e) {
            // эта строчка необходима, чтобы предотвратить стандартное действие браузера
            // в данном случае мы не хотим, чтобы страница перезагружалась при отправке формы
            e.preventDefault();

            // игнорирем создание, если пользователь ничего не ввел в поле
            if(!todoItemForm.input.value) {
                return;
            }

            let todoItem = createTodoItem(todoItemForm.input.value, false, storage);
            storageParse = JSON.parse(localStorage.getItem(storage));
            storageParse.push({
                name: todoItemForm.input.value,
                done: false,
            });
            localStorage[storage] = JSON.stringify(storageParse);
            // создаем и добавляем в список новое дело с названием из поля для ввода
            todoList.append(todoItem.item);
            // обнуляем значение в поле, чтобы не пришлось стирать его в ручную
            todoItemForm.input.value = '';
            
            checkInput();
        });
    }

    window.createTodoApp = createTodoApp;
})();